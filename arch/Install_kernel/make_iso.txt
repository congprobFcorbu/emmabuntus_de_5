# https://www.debian-fr.xyz/viewtopic.php?t=879
# Installer isolinux
# sudo apt-get install syslinux squashfs-tools isolinux syslinux xorriso

# Utilisation version http://ftp.fr.debian.org/debian/dists/bookworm/main/installer-i386/20230427/images/cdrom/
# Utilisation version http://ftp.fr.debian.org/debian/dists/bookworm/main/installer-amd64/20230427/images/cdrom/

# Prendre le fichier initrd.gz se trouvant dans cdrom/gtk

mkdir tmp
cp install/gtk/initrd.gz tmp/initrd.gz
cd tmp
gunzip initrd.gz
mv initrd initrd1
cpio -i -d -H newc --no-absolute-filenames < initrd1
rm initrd1

#changer fond d'écran dans /usr/share/graphics de logo_debian.png et logo_debian_dark.png
cp ../../../../Ecrans/Fond_ecran_demarrage/logo_debian.png  usr/share/graphics/logo_debian.png
cp ../../../../Ecrans/Fond_ecran_demarrage/logo_debian.png  usr/share/graphics/logo_debian_dark.png

# Changer ces valeurs dans le fichier /usr/share/themes/Clearlooks/gtk-2.0/gtkrc
# "#01958b" # homeworld lighter colour par "#689cae" # emmabuntüs lighter colour
# "#01384c" # homeworld darker colour  par "#0c5580" # emmabuntüs darker colour
cp ../../gtkrc usr/share/themes/Clearlooks/gtk-2.0/gtkrc

# Créer nouveau fichier
find . | cpio -H newc -o > ../initrd
cd ..
gzip initrd

cp initrd.gz install/gtk/initrd.gz
rm initrd.gz
rm -R tmp


copier cdrom dans binary/install

copier arch/Install_kernel/32/install ou arch/Install_kernel/64/install dans binary/install

# version 32 bits
xorriso -as mkisofs -r -J -joliet-long -l -cache-inodes -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin -partition_offset 16 -A "Emmabuntus Debian Edition 3" -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o emmabuntus-de3-i686-stretch.iso binary

# version 64 bits
xorriso -as mkisofs -r -J -joliet-long -l -cache-inodes -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin -partition_offset 16 -A "Emmabuntus Debian Edition 3" -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o emmabuntus-de3-amd64-stretch.iso binary
