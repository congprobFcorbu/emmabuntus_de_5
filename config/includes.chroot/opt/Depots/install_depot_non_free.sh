#! /bin/bash

# install_depot_non_free.sh --
#
#   This file permits to install Non-Free Repository.
#
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on XFCE Debian 12.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################



clear

########################################################################################################
# Paramètres de configuration de la distribution
########################################################################################################

nom_distribution="Emmabuntus Debian Edition 5"
base_distribution="bookworm"

repertoire_emmabuntus=~/.config/emmabuntus
file_source="/etc/apt/sources.list"


# Initialisation des URL des dépôts


url_main_repository="http:\\/\\/deb.debian.org\\/debian"
url_updates_repository="http:\\/\\/deb.debian.org\\/debian"
url_security_updates_repository="http:\\/\\/security.debian.org\\/debian-security\\/"



sed s/"^deb\ ${url_main_repository}\ ${base_distribution}\ main\ contrib\$"/"deb ${url_main_repository}\\/ ${base_distribution} main contrib non-free non-free-firmware"/ ${file_source} | sudo tee ${file_source}.tmp

sudo cp ${file_source}.tmp ${file_source}
sudo rm ${file_source}.tmp

sed s/"^deb\ ${url_main_repository}\ ${base_distribution}\ main\$"/"deb ${url_main_repository} ${base_distribution} main contrib non-free non-free-firmware"/ ${file_source} | sudo tee ${file_source}.tmp

sudo cp ${file_source}.tmp ${file_source}
sudo rm ${file_source}.tmp


sed s/"^deb\ ${url_updates_repository}\ ${base_distribution}-updates\ main\ contrib\$"/"deb ${url_updates_repository}\\/ ${base_distribution}-updates main contrib non-free non-free-firmware"/ ${file_source} | sudo tee ${file_source}.tmp

sudo cp ${file_source}.tmp ${file_source}
sudo rm ${file_source}.tmp

sed s/"^deb\ ${url_updates_repository}\ ${base_distribution}-updates\ main\$"/"deb ${url_updates_repository} ${base_distribution}-updates main contrib non-free non-free-firmware"/ ${file_source} | sudo tee ${file_source}.tmp

sudo cp ${file_source}.tmp ${file_source}
sudo rm ${file_source}.tmp

sed s/"^deb\ ${url_security_updates_repository}\ ${base_distribution}-security\ main\ contrib\$"/"deb ${url_security_updates_repository} ${base_distribution}-security main contrib non-free non-free-firmware"/ ${file_source} | sudo tee ${file_source}.tmp

sudo cp ${file_source}.tmp ${file_source}
sudo rm ${file_source}.tmp

sed s/"^deb\ ${url_security_updates_repository}\ ${base_distribution}-security\ main\$"/"deb ${url_security_updates_repository} ${base_distribution}-security main contrib non-free non-free-firmware"/ ${file_source} | sudo tee ${file_source}.tmp

sudo cp ${file_source}.tmp ${file_source}
sudo rm ${file_source}.tmp


