#! /bin/bash


# init_autostart_gspeech.sh --
#
#   This file permits to reinit autostart gSpeech for the Emmabuntüs Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################


clear

repertoire_emmabuntus=~/.config/emmabuntus
fichier_init_config_XFCE=${repertoire_emmabuntus}/init_gspeech_XCFE.txt
fichier_init_config_LXDE=${repertoire_emmabuntus}/init_gspeech_LXDE.txt
fichier_init_config_OpenBox=${repertoire_emmabuntus}/init_gspeech_OpenBox.txt
fichier_init_config_LXQT=${repertoire_emmabuntus}/init_gspeech_LXQT.txt

if ! test -d  ${repertoire_emmabuntus}; then mkdir ${repertoire_emmabuntus}; fi


fichier_init_config=$fichier_init_config_XFCE

if ps -A | grep "xfce4-session"
then
fichier_init_config=$fichier_init_config_XFCE
elif ps -A | grep "lxsession"
then
fichier_init_config=$fichier_init_config_LXDE
elif ps -A | grep "openbox"
then
fichier_init_config=$fichier_init_config_OpenBox
elif ps -A | grep "lxqt-session"
then
fichier_init_config=$fichier_init_config_LXQT
fi

rm ${fichier_init_config}

/opt/gSpeech/autostart_gspeech.sh




