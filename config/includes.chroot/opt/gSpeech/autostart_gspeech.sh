#! /bin/bash


# autostart_gspeech.sh --
#
#   This file permits to autostart gSpeech for the Emmabuntüs Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################


clear

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntus Debian Edition"
nom_logiciel_affichage="gSpeech"
repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config_XFCE=${repertoire_emmabuntus}/init_gspeech_XCFE.txt
fichier_init_config_LXDE=${repertoire_emmabuntus}/init_gspeech_LXDE.txt
fichier_init_config_OpenBox=${repertoire_emmabuntus}/init_gspeech_OpenBox.txt
fichier_init_config_LXQT=${repertoire_emmabuntus}/init_gspeech_LXQT.txt
fichier_init_config_gspeech=~/.config/gSpeech/gspeech.conf

if ! test -d  ${repertoire_emmabuntus}; then mkdir ${repertoire_emmabuntus}; fi


#  chargement des variables d'environnement
. ${env_emmabuntus}

fichier_init_config=$fichier_init_config_XFCE

if ps -A | grep "xfce4-session"
then
fichier_init_config=$fichier_init_config_XFCE
elif ps -A | grep "lxsession"
then
fichier_init_config=$fichier_init_config_LXDE
elif ps -A | grep "openbox"
then
fichier_init_config=$fichier_init_config_OpenBox
elif ps -A | grep "lxqt-session"
then
fichier_init_config=$fichier_init_config_LXQT
fi

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)

if [[ ! ( -f $fichier_init_config) ]]
then

   echo "# $nom_distribution" > $fichier_init_config



    message_affichage="\n\
    \<span color=\'${highlight_color}\'>$(eval_gettext 'Do you want to launch gSpeech (Text to Speech) at system startup?')\</span>\n\
    \n\
    $(eval_gettext 'This application is also accessible via the dock, and the drop down menu at the top left.')\n\
    \n\
    \<span color=\'"'red\'"'>$(eval_gettext 'Warning:')\</span> $(eval_gettext 'We advise against using gSpeech on computers with less than 512MB of RAM     ')\n\
    $(eval_gettext 'in order to minimize the memory usage.')\n\
    \n\
    $(eval_gettext 'If you want later to turn this feature on at every boot,')\n\
    $(eval_gettext 'please run in a Terminal the command: init_autostart_gspeech')"




    export WINDOW_DIALOG_GSPEECH='<window title="'${nom_logiciel_affichage}'" icon-name="gtk-info" resizable="false">
    <vbox spacing="0">

    <text use-markup="true" wrap="false" xalign="0" justify="3">
    <input>echo "'${message_affichage}'" | sed "s%\\\%%g"</input>
    </text>

    <hbox spacing="10" space-expand="false" space-fill="false">
    <button cancel></button>
    <button can-default="true" has-default="true" use-stock="true" is-focus="true">
    <label>gtk-ok</label>
    <action>exit:OK</action>
    </button>
    </hbox>

    </vbox>
    </window>'

    MENU_DIALOG_GSPEECH="$(gtkdialog --center --program=WINDOW_DIALOG_GSPEECH)"

    eval ${MENU_DIALOG_GSPEECH}
    echo "MENU_DIALOG_GSPEECH=${MENU_DIALOG_GSPEECH}"


    if [ ${EXIT} == "OK" ]
    then

      echo "# $nom_logiciel_affichage enabled" >> $fichier_init_config
      echo "# $nom_logiciel_affichage enabled"

      if ps -A | grep "xfce4-session"
      then

         sed s/"gSpeech=\"[0-9]*\""/"gSpeech=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

      elif ps -A | grep "lxsession"
      then

         sed s/"gSpeech_LXDE=\"[0-9]*\""/"gSpeech_LXDE=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

      elif ps -A | grep "openbox"
      then

         sed s/"gSpeech_OpenBox=\"[0-9]*\""/"gSpeech_OpenBox=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

      elif ps -A | grep "lxqt-session"
      then

         sed s/"gSpeech_LXQT=\"[0-9]*\""/"gSpeech_LXQT=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

      fi

      cp ${env_emmabuntus}.tmp ${env_emmabuntus}
      rm ${env_emmabuntus}.tmp

      if test -f ~/.cache/gSpeech/gspeech.pid
      then
        rm ~/.cache/gSpeech/gspeech.pid
      fi

      kill -9 $(pgrep gspeech)
      gspeech &

    else

      echo "# $nom_logiciel_affichage diseabled" >> $fichier_init_config
      echo "# $nom_logiciel_affichage diseabled"

      if test -f ~/.cache/gSpeech/gspeech.pid
      then
        rm ~/.cache/gSpeech/gspeech.pid
      fi
      kill -9 $(pgrep gspeech)


      if ps -A | grep "xfce4-session"
      then

         sed s/"gSpeech=\"[0-9]*\""/"gSpeech=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

      elif ps -A | grep "lxsession"
      then

         sed s/"gSpeech_LXDE=\"[0-9]*\""/"gSpeech_LXDE=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

      elif ps -A | grep "openbox"
      then

         sed s/"gSpeech_OpenBox=\"[0-9]*\""/"gSpeech_OpenBox=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

      elif ps -A | grep "lxqt-session"
      then

         sed s/"gSpeech_LXQT=\"[0-9]*\""/"gSpeech_LXQT=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

      fi

      cp ${env_emmabuntus}.tmp ${env_emmabuntus}
      rm ${env_emmabuntus}.tmp


   fi

else

   if [[ -f ${fichier_init_config_gspeech} ]]
   then

   language_gspeech="fr-FR"

       if [[ $LANG == fr* ]] ; then

           language_gspeech="fr-FR"

       elif [[ $LANG == it* ]] ; then

           language_gspeech="it-IT"

       elif [[ $LANG == es* ]] ; then

           language_gspeech="es-ES"

       elif [[ $LANG == de* ]] ; then

           language_gspeech="de-DE"

       elif [[ $LANG == en_GB* ]] ; then

           language_gspeech="en-GB"

       else

           language_gspeech="en-US"
       fi

       if [[ $(grep -e ${language_gspeech} ${fichier_init_config_gspeech}) ]]
       then

           echo "Langue ${language_gspeech} ready"

       else

           # change language
           echo "Change Langue to ${language_gspeech} "
           sed s/defaultlanguage[^$]*/defaultlanguage\ =\ ${language_gspeech}/ ${fichier_init_config_gspeech} > ${fichier_init_config_gspeech}.tmp
           cp ${fichier_init_config_gspeech}.tmp ${fichier_init_config_gspeech}
           rm ${fichier_init_config_gspeech}.tmp

       fi



   fi

   if ps -A | grep "xfce4-session"
   then

      if [[ $gSpeech == "1" ]] ; then
         if test -f ~/.cache/gSpeech/gspeech.pid
         then
            rm ~/.cache/gSpeech/gspeech.pid
         fi
         kill -9 $(pgrep gspeech)
         gspeech &
      fi

   elif ps -A | grep "lxsession"
   then

      if [[ $gSpeech_LXDE == "1" ]] ; then
         if test -f ~/.cache/gSpeech/gspeech.pid
         then
            rm ~/.cache/gSpeech/gspeech.pid
         fi
         kill -9 $(pgrep gspeech)
         gspeech &
      fi

   elif ps -A | grep "openbox"
   then

      if [[ $gSpeech_OpenBox == "1" ]] ; then
         if test -f ~/.cache/gSpeech/gspeech.pid
         then
            rm ~/.cache/gSpeech/gspeech.pid
         fi
         kill -9 $(pgrep gspeech)
         gspeech &
      fi

   elif ps -A | grep "lxqt-session"
   then

      if [[ $gSpeech_LXQT == "1" ]] ; then
         if test -f ~/.cache/gSpeech/gspeech.pid
         then
            rm ~/.cache/gSpeech/gspeech.pid
         fi
         kill -9 $(pgrep gspeech)
         gspeech &
      fi

   fi

fi

exit 0





