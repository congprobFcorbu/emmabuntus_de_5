#! /bin/bash

# install_logiciel_non_libre.sh --
#
#   This file permits to install non-free softwares for the Emmabuntus Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

# Remarque surpression des "--timeout=$delai_fenetre_selection" dans les commandes zenity avec test en sortie


clear

nom_distribution="Emmabuntus Debian Edition 5"
base_distribution="bookworm"

nom_logiciel_affichage="Install non-free Softwares"

dir_install_non_free_softwares=/opt/Install_non_free_softwares
fichier_init_config=${dir_install_non_free_softwares}/.init_config_non_free_soft_all.txt

cde_paquet_sources_list="deb http://www.deb-multimedia.org ${base_distribution} main non-free"
sources_list="/etc/apt/sources.list"

dir_install_fonts_arial=/usr/share/fonts/truetype/msttcorefonts/Arial.ttf
dir_install_fonts_calibri=/usr/share/fonts/truetype/fonts_MS_Office_2007/Calibri.ttf
delai_fenetre=20
delai_fenetre_selection=120
delai_fenetre_progression=1200 ; # temporisation de 20 minutes
choix=""
user=""
utilisateur=emmabuntus

if [[ $LANG == fr* ]]
then
    nom_logiciel_affichage="Installation des logiciels non libres"
fi


#  chargement des messages
. /opt/Install_non_free_softwares/emmabuntus_messages.sh


# Récupération des arguments après le passage en root
choix=$1
user=$2

echo "choix=$choix"
echo "user=$user"


bureau=$XDG_DESKTOP_DIR

if [ "$choix" ]
then

## 1 ème barre de progression zenity à cause d'une fermeture de celle-ci
## dû à la commande apt update

(

    #Désactivation de Gmenu de Cairo-dock pendant l'installation de logiciel pour éviter les fenêtres de notifications
    /usr/bin/cairo_dock_gmenu_off.sh
    # Définition des dépôts à ajouter, car non présent dans le fichier de config (Gros BUG !!!)

    /opt/Depots/install_depot_emmabuntus.sh

    # Définition des dépôts à ajouter

    if [[ $choix == *Codecs* ]]
    then

        /opt/Depots/install_depot_non_free.sh

    fi


    echo "# $msg_repository_update"

    # Permet de vérifier la présence des paquets de langue
    if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then

        echo "Internet is a live"

        # Suppression de la surveillance de la mise à jour des paquets
        pkill pk-update-icon

        echo "# $msg_repository_update"
        sudo apt-get -qq update
    fi

    echo "100"
    ) |
    zenity --progress --pulsate \
      --title="$install_title" \
      --text="$install_text" \
      --width=500 \
      --percentage=0 --auto-close --no-cancel\
      --timeout=$delai_fenetre_progression
        if [ $? = "1" ]
        then
          sudo dpkg --configure -a

          #Activation de Gmenu de Cairo-dock
          /usr/bin/cairo_dock_gmenu_on.sh

          zenity --error --timeout=$delai_fenetre \
            --text="$install_cancel"
        else
          sudo dpkg --configure -a

          #Activation de Gmenu de Cairo-dock
          /usr/bin/cairo_dock_gmenu_on.sh

        fi


        # Installation des logiciels non libres


        ## 4 ème barre de progression zenity

        (

        if [[ $choix == *Codecs* ]]
        then
            nom_logiciel_affichage="Codecs"
            nom_paquet=libdvdcss2

            if [[ $(dpkg --get-selections ${nom_paquet} 2> /dev/null | grep -e "install") ]]
            then

               echo "# $nom_logiciel_affichage $msg_installed"

            else
                echo "# $msg_install $nom_logiciel_affichage"

                # non-free-codecs  libdvdcss2

                if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then
                echo "Internet is a live"
                    sudo gdebi -n -q ${dir_install_non_free_softwares}/Codecs/gstreamer1.0-fluendo-mp3*.deb
                    sudo apt-get install -y -qq regionset
                    sudo apt-get install -y -qq unrar
                else
                    sudo gdebi -n -q ${dir_install_non_free_softwares}/Codecs/gstreamer1.0-fluendo-mp3*.deb
                    sudo gdebi -n -q ${dir_install_non_free_softwares}/Codecs/regionset*.deb
                    sudo gdebi -n -q ${dir_install_non_free_softwares}/Codecs/unrar*.deb
                fi

                if [[ $(uname -m | grep -e "x86_64") ]] ; then
                    sudo gdebi -n -q ${dir_install_non_free_softwares}/Codecs/w64codecs*.deb
                else
                    sudo gdebi -n -q ${dir_install_non_free_softwares}/Codecs/w32codecs*.deb
                fi


                    sudo gdebi -n -q ${dir_install_non_free_softwares}/libdvdcss2*.deb

            fi

        fi


        if [[ $choix == *Arial* ]]
        then

            nom_logiciel_affichage="Arial Microsoft"
            nom_paquet=${dir_install_non_free_softwares}/install_microsoft_fonts.sh

            if test -f ${dir_install_fonts_arial}
            then
                echo "# $nom_logiciel_affichage $msg_installed"

            else

                echo "# Install $nom_logiciel_affichage"

                ${nom_paquet}

            fi


        fi

        if [[ $choix == *Calibri* ]]
        then

            nom_logiciel_affichage="Calibri Microsoft"
            nom_paquet=${dir_install_non_free_softwares}/install_calibri_fonts.sh

            if test -f ${dir_install_fonts_calibri}
            then
                echo "# $nom_logiciel_affichage $msg_installed"

            else

                echo "# Install $nom_logiciel_affichage"

                ${nom_paquet}

            fi


        fi

        if [[ $choix == *Codecs* ]]
        then
            nom_logiciel_affichage="Cleaning ..."

            sudo apt-get -y -qq autoclean
            sudo apt-get -y -qq clean

        fi

    #Activation de Gmenu de Cairo-dock
    /usr/bin/cairo_dock_gmenu_on.sh

    echo "100"
    ) |
    zenity --progress --pulsate \
      --title="$install_title" \
      --text="$install_text" \
      --width=500 \
      --percentage=0 --auto-close --no-cancel\
      --timeout=$delai_fenetre_progression
        if [ $? = "1" ]
        then
          sudo dpkg --configure -a

          #Activation de Gmenu de Cairo-dock
          /usr/bin/cairo_dock_gmenu_on.sh

          zenity --error --timeout=$delai_fenetre \
            --text="$install_cancel"
        else
          sudo dpkg --configure -a

          #Activation de Gmenu de Cairo-dock
          /usr/bin/cairo_dock_gmenu_on.sh

        fi


fi

sudo chmod o+r /var/lib/command-not-found/commands.db*

exit 0



