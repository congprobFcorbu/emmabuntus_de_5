#! /bin/bash

# calamares_oem_preinstall_exec.sh --
#
#   This file permits to prepare OEM install for the Emmabuntus Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

file_config="/etc/calamares/settings.conf"
mount_point="/media/system_patch_hostname"

if [[ -f ${file_config}.preoem ]] ; then

    pkill cairo-dock

    if [[ -f ${file_config} ]] ; then
        sudo mv ${file_config} ${file_config}.bak
    fi

    sudo cp ${file_config}.preoem ${file_config}

    sudo install-debian

    if [[ -f ${file_config}.bak ]] ; then
        sudo rm ${file_config}
        sudo mv ${file_config}.bak ${file_config}
    fi

    # Patch hostname
    sudo mkdir ${mount_point}
    sudo mount /dev/sda1 ${mount_point}

    if [[ $(cat ${mount_point}/etc/hostname | grep localhost.localdomain) ]] ; then

        echo "oem-pc" | sudo tee ${mount_point}/etc/hostname

        sudo tee ${mount_point}/etc/hosts > /dev/null <<EOT
127.0.0.1   localhost
127.0.0.1   oem-pc.unassigned-domain oem-pc

# The following lines are desirable for IPv6 capable hosts
::1         localhost ip6-localhost ip6-loopback
ff02::1     ip6-allnodes
ff02::2     ip6-allrouters
EOT

    fi

    sudo umount ${mount_point}
    sudo rm -d ${mount_point}

    exit 0

else

    exit 1

fi



