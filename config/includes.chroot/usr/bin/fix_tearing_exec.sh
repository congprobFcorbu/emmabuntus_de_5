#! /bin/bash


# fix_tearing_exec.sh --
#
#   This file permits to fix tearing for the Emmabuntüs Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################


nom_distribution="Emmabuntus Debian Edition 5"
dir_config=/etc/X11/xorg.conf.d
file_config_intel=20-intel.conf
file_config_amd=20-amd.conf


if [[ $(lspci -G | grep -i graphics | egrep -i "intel|amd") ]] ; then

    if [[  ! ( -f ${dir_config}/${file_config_intel} || -f ${dir_config}/${file_config_amd} ) ]] ; then

        if ! test -d ${dir_config} ; then
            sudo mkdir ${dir_config}
            sudo chmod -R a+rX ${dir_config}
        fi

        if [[ $(lspci -G | grep -i graphics | grep -i intel) ]] ; then

            echo -e 'Section "Device"\n Identifier "Intel Graphics"\n Driver "Intel"\n #Option "AccelMethod" "sna"\n Option "TearFree" "true"\nEndSection' | sudo tee ${dir_config}/${file_config_intel}

        elif [[ $(lspci -G | grep -i graphics | grep -i amd) ]] ; then

            echo -e 'Section "Device" Identifier "Radeon" Driver "radeon" Option "TearFree" "on" EndSection' | sudo tee ${dir_config}/${file_config_amd}

        fi

    fi

fi
